<?php
	@ob_start();
	session_start();
	if(isset($_POST['proses'])){
		require 'config.php';
			
		$user = strip_tags($_POST['user']);
		$pass = strip_tags($_POST['pass']);

		$sql = 'select * from member inner join login join roles on member.id_member = login.id_member and login.id_role = roles.id where user =? and pass = md5(?);';

		$row = $config->prepare($sql);
		$row -> execute(array($user,$pass));
		$jum = $row -> rowCount();
		if($jum > 0){
			$hasil = $row -> fetch();
			$_SESSION['role_name'] = $hasil['role_name'];
			$_SESSION['admin'] = $hasil;
			echo '<script>alert("Login Sukses");window.location="index.php"</script>';
		}else{
			echo '<script>alert("Login Gagal");history.go(-1);</script>';
		}
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login - Kasir APP</title>
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="sb-admin/css/sb-admin-2.min.css" rel="stylesheet">
	<link href="sb-admin/css/custom.css" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
</head>

<body>
	<img class="wave" src="assets/img/wave.png" />
		<div class="container">
		<div class="img">
			<img src="assets/img/bg.svg" />
		</div>
		<div class="login-content">
			<form method="POST">
			<img src="assets/img/logo.png">
			<h2 class="title">Welcome to Kasir App</h2>
			<div class="input-div one">
				<div class="i">
				<i class="fas fa-user"></i>
				</div>
				<div class="div">
				<h5>Username</h5>
				<input type="text" class="input" name="user"/>
				</div>
			</div>
			<div class="input-div pass">
				<div class="i">
				<i class="fas fa-lock"></i>
				</div>
				<div class="div">
				<h5>Password</h5>
				<input type="password" class="input" name="pass"/>
				</div>
			</div>
			<input type="submit" class="btn" value="Login" name="proses" />
			</form>
		</div>
		</div>
		<!-- Bootstrap core JavaScript-->
		<script src="sb-admin/vendor/jquery/jquery.min.js"></script>
		<script src="sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<!-- Core plugin JavaScript-->
		<script src="sb-admin/vendor/jquery-easing/jquery.easing.min.js"></script>
		<!-- Custom scripts for all pages-->
		<script src="sb-admin/js/sb-admin-2.min.js"></script>
		<script src="sb-admin/js/custom.js"></script>
</body>
</html>