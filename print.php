<?php 
	require 'config.php';
	include $view;
	$lihat = new view($config);
	$toko = $lihat -> toko();
	$hsl = $lihat -> penjualan();
?>
<html>
	<head>
		<title>print</title>
		<link rel="stylesheet" href="assets/css/bootstrap.css">
	</head>
	<body>
		<script>window.print();</script>
		<div class="container">
			<div class="row">
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
					<center>
						<p><?php echo $toko['nama_toko'];?></p>
						<p><?php echo $toko['alamat_toko'];?></p>
						<p>Tanggal : <?php  echo date("j F Y, G:i");?></p>
						<p>Kasir : <?php  echo $_GET['nm_member'];?></p>
					</center>
					<table class="table table-bordered" style="width:100%;">
						<tr>
							<td>No.</td>
							<td>Barang</td>
							<td>Jumlah</td>
							<td>Total</td>
						</tr>
						<?php $no=1; foreach($hsl as $isi){?>
						<tr>
							<td><?php echo $no;?></td>
							<td><?php echo $isi['nama_barang'];?></td>
							<td class="text-center"><?php echo $isi['jumlah'];?></td>
							<td>Rp <?php echo number_format ($isi['total']);?>,-</td>
						</tr>
						<?php $no++; }?>
						<tr>
						<?php $hasil = $lihat -> jumlah(); ?>
							<td>Total</td>
							<td></td>
							<td></td>
							<td>Rp <?php echo number_format($hasil['bayar']);?>,-</td>
						</tr>
						<tr>
							<td>Bayar</td>
							<td></td>
							<td></td>
							<td>Rp <?php echo number_format($_GET['bayar']);?>,-</td>
						</tr>
						<tr>
							<td>Kembali</td>
							<td></td>
							<td></td>
							<td>Rp <?php echo number_format($_GET['kembali']);?>,-</td>
						</tr>
					</table>
					<div class="clearfix"></div>
					<center>
						<p>Terima kasih telah berbelanja di toko kami !</p>
					</center>
				</div>
				<div class="col-sm-4"></div>
			</div>
		</div>
	</body>
</html>
